import React, { FunctionComponent } from "react";
import { Link } from "react-router-dom";
import "./NavBarLink.scss";

const NavBarLink: FunctionComponent = (props) => {
  const { children } = props;
  return (
    <Link to="/">
      {children}
    </Link>
  );
};

export default NavBarLink;