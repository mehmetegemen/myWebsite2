import React, { FunctionComponent } from "react";
import "./HeroBackground.scss";

const HeroBackground: FunctionComponent = (props) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1428.91 699.78">
      <defs>
        <linearGradient id="a" x1="964.3" y1="664.08" x2="310.41" y2="-411.93" gradientUnits="userSpaceOnUse">
          <stop offset="0" stopColor="#d1df4f"/>
          <stop offset="1" stopColor="#889739"/>
        </linearGradient>
        <linearGradient id="b" x1="1105.08" y1="681.1" x2="792.25" y2="166.31" gradientUnits="userSpaceOnUse">
          <stop offset="0" stopColor="#4db9e0"/>
          <stop offset="1" stopColor="#04677c"/>
        </linearGradient>
      </defs>
      <g data-name="Layer 2">
        <g data-name="Layer 1">
          <path d="M1428.91 0v310.73a123.09 123.09 0 0 1-36 87l-119 119-147.01 147a123 123 0 0 1-84.74 36H228.37a123.09 123.09 0 0 1-87-36L36.05 558.45A123.05 123.05 0 0 1 0 471.32L.34 0z" fill="url(#a)"/>
          <path d="M1273.81 516.82L1126.9 663.73a123 123 0 0 1-84.74 36H727.55l-4.36-71.55s56-82.48 194.4-111.93l14.73-50.08s-97.2-185.54 8.84-315.17l-41.24-29.46S920.54 56.77 1041.3 45c0 0 53 73.64 70.69 67.75 0 0 129.6-23.57 64.8 335.78l29.46-5.89 20.62 44.18s16.88 16.87 46.94 30z" fill="url(#b)"/>
        </g>
      </g>
    </svg>
  );
};

export default HeroBackground;
