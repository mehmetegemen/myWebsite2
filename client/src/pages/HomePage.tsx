import React, { FunctionComponent, useState } from "react";
import GitHubIcon from "../components/Icons/Github";
import HeroBackground from "../components/HeroBackground/HeroBackground";
import MediumIcon from "../components/Icons/Medium";
import MidWavy from "../components/MidWavy/MidWavy";
import NavBar from "../components/NavBar/NavBar";

import "./HomePage.scss";

const HomePage: FunctionComponent = (props) => {
  const [ contactToggle, setContactToggle ] = useState(false);
  let contactElem;
  if (contactToggle) {
    contactElem = (<>
      Email: mehmetegemenalbayrak@gmail.com
      <br/> Whatsapp & Phone: +90 541 261 59 37
    </>);
  } else {
    contactElem = (<>
      Click me to see the contact details
    </>);
  }
  const contactKeyPress = (e: any) => {
    if (e.key === "Enter") {
      e.target.click();
    }
  }
  return (
    <div>
      <div className="top-container">
        <HeroBackground />
        <div className="top-container__content">
          <NavBar />
          <div className="top-container__content__left">
            <h1>Hardworking.</h1>
            <h1>Diligent.</h1>
            <p>And compassionate. I care about people and live for people.
              Feeling responsible for high quality and delivering it.
            </p>
          </div>
        </div>
      </div>
      <div className="about-me">
        <img src="inp-original-min.jpg" className="my-image" />
        <p className="about-me__content">
          I am Mehmet Egemen, a full-fledged generalist. Data science to backend,
          frontend, mobile development or illustration to support my designs; marketing
          and sales to set up my future startup, I touch everywhere. I have been programming
          since 8 and this experience gave me an intrinsic ability, intuition
          which leads me magically most of the times.
        </p>
      </div>
      <div className="works">
        <div className="works__medium">
          <div className="works__medium__icon">
            <MediumIcon />
          </div>
          <p>
            I like to write and interact with that excuse. I like to share. Here on Medium I write
            about data science and programming. I read about software engineering,
            computer science, personal improvement, etc. and many other things.
            You can follow me to read about anything you can think of.
            <a href="https://medium.com/@mehmetegemenalbayrak/">
            Click here to see my blog posts on medium.</a>
          </p>
        </div>
        <div className="works__github">
          <div className="works__github__icon">
            <GitHubIcon />
          </div>
          <p>
            I don't stop coding. My favorite languages are Javascript, Typescript, and Python.
            I constantly experiment with them and keep myself fresh. So my github takes its share of it.
            Time to time I upload boilerplates or beginner guiding snippets, you can take a look!
            <a href="https://github.com/mehmetegemen">Click here to see my repositories.</a>
          </p>
        </div>
      </div>
      <div className="more-about-me">
        <div>
          <h1>My personality...</h1>
          <p>
          I am a person of purpose. My desire is to help people in need. I would like to
          defy against self-centered gears of life, that's why I always share and help.
          I believe there is much to be solved in this world and I
          would like to solve them. So problem-solving is something I do enthusiastically.
          My personal attributes which help me along that way are:
          </p>
          <ul>
            <li>Focus</li>
            <li>Patience</li>
            <li>Dedication</li>
            <li>Honesty</li>
          </ul>
        </div>
        <img src="mountain_water.jpg" />
      </div>
      <div className="contact">
        <MidWavy />
        <div className="contact__content">
          <p>Do you want to work with me? Do you want to ask something? 
            Okay then, you can always reach me. 
          </p>
          <div
            tabIndex={0}
            onClick={() => {setContactToggle(true)}}
            onKeyPress={contactKeyPress}
          >
            {contactElem}
          </div>
        </div>
      </div>
    </div>
  );
};

export default HomePage;
